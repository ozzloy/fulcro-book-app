(ns app.application
  (:require
    [com.fulcrologic.fulcro.application :as app]))

(defonce app (app/fulcro-app))

(def letter-range 15)

(def bingo "b1ng0")

(defn make-master-column [top n letter-range]
  {(nth top n)
   (map inc (range (* n letter-range)
                   (* (+ n 1) letter-range)))})

(defn make-master-card [top letter-range]
  (into (array-map)
        (for [i (range(count top))]
          (make-master-column top i letter-range))))

(def master-card (make-master-card "bingo" 15))

(defn transpose [m] (apply mapv vector m))

(defn card-content [card]
  (for [[key value] card] value))
(defn card-row [card row]
  (nth (transpose (card-content card)) row))

(defn row-display-data [card row]
  [:tr
   (for [n (-> card card-content transpose (nth row))] [:td n])])
(defn card-content-display-data [card]
  (for [row (-> card card-content (nth 0) count range)]
    (row-display-data card row)))
(defn card-header-display-data [card]
  [:tr
   (for [[header column] card] [:th header])])

(defn make-player-card [master-card]
  (into (array-map)
        (for [[head column] master-card]
          {head (take (count bingo) (shuffle column))})))

(defn card-display-data [card]
  [:table
   (card-header-display-data card)
   (card-content-display-data card)])
